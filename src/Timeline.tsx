import React from 'react';

interface Props
{
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class Timeline extends React.Component<Props,State>
{
    render()
    {
        return (
<div className="timeline">
    { this.props.children }
</div>
        );
    }
};
export default Timeline;
