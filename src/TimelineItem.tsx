import React from 'react';

interface Props
{
    time,
    title,
    color?,
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class TimelineItem extends React.Component<Props,State>
{
    render()
    {
        const style:any = {
            '--color': this.props.color,
        };
        return (
<div className="timeline-item" style={style}>
    <div className="timeline-item-header">
        <div className="timeline-item-header-title">
            { this.props.title }
        </div>
        <div className="timeline-item-header-datetime">
            { this.props.time }
        </div>
    </div>
    <div className="timeline-item-content">
        { this.props.children }
    </div>
</div>
        );
    }
};
export default TimelineItem;
